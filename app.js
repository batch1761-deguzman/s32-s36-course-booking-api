// [SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const cors = require('cors');
	const userRoutes = require('./routes/users');
	const courseRoutes = require('./routes/courses')  

// [SECTION] Environment Setup
	dotenv.config(); 
	let account = process.env.CREDENTIALS;
	const port = process.env.PORT; 

// [SECTION] Server Set Up
	const app = express();
	app.use(express.json());
	// it enables all origins/address/URL of the client request
	app.use(cors());

// [SECTION] Database Connection
	mongoose.connect(account);
	const connectStatus = mongoose.connection;
	connectStatus.once('open', () => console.log('Database Connected'));


// [SECTION] Backend Routes
	// http://localhost:4000/users
	app.use('/users',userRoutes); 
	// http://localhost:4000/courses
	app.use('/courses',courseRoutes);


// [SECTION] Server gateway Response
	app.get('/', (req, res) => {
		res.send('Welcome to Enrollment');
	})
	app.listen(port, () => {
		console.log(`API is Hosted port ${port}`);
	});