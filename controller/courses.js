const Course = require('../models/Course');

// Create a new course
/*
	Steps:
		1. Create a new Course object using the mongoose model and the information from the request body.
		2. save the new course to the database
*/
module.exports.addCourse = (reqBody) => {
	// create a variable "newCourse" and instantiate the name, description, price
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});
	// save the created object to our databas
	return newCourse.save().then((course, error) => {
		if (error) {
			return false;
		} else {
			return course;
		};
	}).catch(error => error.message);
};

// retieve all courses

// 1. Retrieve all the courses from the database

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
};

// Retrieve all ACTIVE courses
// 1. Retrieve all the courses with the property isActive: true

//users who arent admin or arent logged in should be able to view 
module.exports.getAllActive = () => {
	return Course.find({ isActive: true}).then(result => {
		return result;
	}).catch(error => error)
};

// Retrieving a specific course
// 1. Retrieve the course that matches the course ID provided from the URL

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result;
	}).catch(error => error)
};

// UPDATE a course
/*
	Steps:
		1. Create a variable "updatedCourse" which will contain the info retrieved from the req.body
		2. Find and Update the course using the courseId retrieved from the req.params and the variable "updatedCourse" containing info from req.body
*/

module.exports.updateCourse = (courseId, data) => {
	// specify the fields or properties of the document to be updated
	let updatedCourse = {
		name:data.name,
		description: data.description,
		price: data.price
	};

	// findbyIdAndUpdate(document Id, updateToBeApplied)
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};

// Archiving a course
// 1. update the status of "isActive" into "false" which will no longer be displayed in the client whenever all active courses are retrieved

module.exports.archiveCourse = (courseId) => {
	let updateActivefield = {
		isActive: false
	};

	return Course.findByIdAndUpdate(courseId, updateActivefield).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	}).catch(error => error);
};


// Activate a course
// 1. update the status of "isActive" into "true" 

module.exports.activateCourse = (courseId) => {
	let updateActivefield = {
		isActive: true
	};

	return Course.findByIdAndUpdate(courseId, updateActivefield).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	}).catch(error => error);
};


// to delete (findbyIdAndRemove)

