// [SECTION] Dependencies and modules
	const User = require('../models/User');
	const Course = require('../models/Course');  
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');
	const auth = require('../auth.js');

// [SECTION] Environment Variables Setup
	dotenv.config();
	const asin = parseInt(process.env.SALT);

// [SECTION] Functionalities [CREATE] 
	// 1. Register New Account
	module.exports.register = (userData) => {
		let fName = userData.firstName;
		let lName = userData.lastName;
		let email = userData.email;
		let passW = userData.password;
		let mobil = userData.mobileNo;
		
		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, asin),
			mobileNo: mobil
		});
		
		return newUser.save().then((user, err) => {
			if (user) {
				return user;
			} else {
				return {message: 'Failed to Register account'};
			};
		}); 
	};

// [SECTION] User Authentication
	/*
		Steps:
		 1. Check the database if the user email exists
		 2. Compare the password provided in the login form with the password stored in the database.
		 3. Generate/Return a JSON web token if the user is successfully logged in, and return false if not
	*/

	module.exports.loginUser = (data) => {
		// findOne method returns the first record in the collectionthat matches the search criteria
		return User.findOne({email: data.email}).then(result => {
			// user  does not exist

			if (result == null) {
				return false;
			} else {
				// user exists
				// compareSync' method from the bcrypt to use in comparing the non encrypted password from the logun and the database password. it returns 'true' or 'false' depending on the result
				const isPasswordCorrect = bcrypt.compareSync(data.password, result.password);

				// if the password match, return token
				if (isPasswordCorrect) {
					return {accessToken: auth.createAccessToken(result.toObject())}
				} else {
					// password fo not match
					return false;
				}

			}
		});
	};

// [SECTION] Functionalities [RETRIEVE] (user details)
	/*
		Steps:
			1. find the document ni the database using the user's ID
			2. reassign the password of the return document to an empty string
			3. return the result back to the client
	*/
	module.exports.getProfile = (data) => {
		return User.findById(data).then(result => {
			// change value of the user password to an empty string
			result.password = '';
			return result;
		});
	};


	// Enroll registered user
	/*
		Enrollment Steps
		1. Look for the user by its ID
			- push the details of the course we're trying to entroll in. We'll push the details to a new enrollment subdocument in our user.
		2. look for the course by its ID.
			- push the details of the enrolee/user who's trying to enroll.
			we'll push to a new enrollees subdocument in our course.

		3. When both saving documents are successful, we send a message to a client. true if successful, false if not.
	*/
	module.exports.enroll = async (req, res) => {
		// console.log('test enroll route');
		console.log(req.user.id);//the user's id from the decoded token after verifying
		console.log(req.body.courseId) //course ID from our request body

		// Process stops here and dens response if user is an admin
		if (req.user.isAdmin) {
			return res.send({message: "action Forbidden"})
		}

		// get the use's ID to save the courseId inside the enrollments field

		let isUserUpdated = await User.findById(req.user.id).then( user => {
			// add the courseId in an object and push that object into user's enrollment array;

			let newEnrollment = {
				courseId: req.body.courseId
			}

			user.enrollments.push(newEnrollment);

			// save the changes made to our user documents
			return user.save().then(user => true).catch(err => err.message)

			// if isUserUpdated does not contain the boolean true, we will stop our process and return a message to our client
			if(isUserUpdated !== true) {
				return res.send({ message: isUserUpdated });
			}

		});

		// find the course id that we will need to push to out enrollee
		let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
			// create an object which will be pushed to enrolles array/field
			let enrollee = {
				userId: req.user.id
			}

			course.enrollees.push(enrollee);
			// save the course document
			return course.save().then(course => true).catch(err => err.message)

			if(isCourseUpdated !== true){
				return res.send({message: isCourseUpdated})
			}
		});

		// send message to the client that we have successfully entrolled our user if both isuserupdated and isCourseUpdated contains the boolean true

		if(isUserUpdated && isCourseUpdated) {
			return res.send ({message: "enrolled successfully"})
		};

	};

	// get user enrollment

	module.exports.getEnrollments = (req, res) => {
		User.findById(req.user.id)
		.then(result => res.send(result.enrollments))
		.catch(err => res.send(err))
	}

// [SECTION] Functionalities [UPDATE]

// [SECTION] Functionalities [DELETE]