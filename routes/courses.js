const express = require('express');
const route = express.Router();
const CourseController = require('../controller/courses');
const auth = require('../auth');

// destructure the actual function that we need to use

const {verify, verifyAdmin} = auth;

// Route for creating a route
route.post('/create', verify, verifyAdmin, (req, res) => {
	CourseController.addCourse(req.body).then(result => res.send(result))
});


// Retrieve all courses (admin)
route.get('/all', (req, res) => {
	CourseController.getAllCourses().then(result => res.send(result));
});


// Retrieve all Active courses
route.get('/active', (req, res) => {
	CourseController.getAllActive().then(result => res.send(result));
});

// retrieve a SPECIFIC course
// req.params (is short for a parameter)
// "/:parameterName" wildcard
route.get('/:courseId', (req, res) => {
	console.log(req.params.courseId);
	//we can retrieve the course ID by accessing the request's "params" property which contains all the paramteres provided via the URL
	CourseController.getCourse(req.params.courseId).then(result => res.send(result));
});

// Route for UPDATING a course
route.put('/:courseId', verify, verifyAdmin, (req, res) => {
	CourseController.updateCourse(req.params.courseId, req.body).then(result => res.send(result));
});


// Archiving a course

route.put('/:courseId/archive', verify, verifyAdmin, (req, res) => {
	CourseController.archiveCourse(req.params.courseId).then(result => res.send(result));
});

// Activate a course

route.put('/:courseId/activate', verify, verifyAdmin, (req, res) => {
	CourseController.activateCourse(req.params.courseId).then(result => res.send(result));
});






module.exports = route;