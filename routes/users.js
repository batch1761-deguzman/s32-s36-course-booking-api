// [SECTION] Dependencies and Modules
	const exp = require('express'); 
	const controller = require('../controller/users'); 
	//  we will import auth module so we can verify method as middleware for our routes
	const auth = require('../auth')

// [SECTION] Routing Component
	const route = exp.Router();

// [SECTION] Routes - POST
	route.post('/register', (req, res) => {
		console.log(req.body);
		let userData = req.body;
		controller.register(userData).then(outcome => {
			res.send(outcome);
		});
	});
	
// [SECTION] Routes for User Aithentication(login)

	route.post('/login', (req, res) => {
		controller.loginUser(req.body).then(result => res.send(result));
	});

// [SECTION] Routes - GET (user details)
	route.get('/details', auth.verify, (req,res) => {
		controller.getProfile(req.user.id).then(result => res.send(result));
	})


// Enroll our registered users
// only the verified user can enroll in a course
route.post('/enroll', auth.verify, controller.enroll); //method using controller instead of the ususal request response

//Get logged user's enrollments
route.get('/getEnrollments', auth.verify, controller.getEnrollments);

// [SECTION] Routes- PUT
// [SECTION] Route - DEL
// [SECTION] Expose Route System
	module.exports = route;